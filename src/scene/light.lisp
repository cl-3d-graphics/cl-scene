;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

;;; --------------------------------------
;;; These also serve as Light.Type in GLSL
;;; --------------------------------------
(defconstant *ambient-light*     0)
(defconstant *omni-light*        1)
(defconstant *directional-light* 2)
(defconstant *spot-light*        3)

;;; -----------------
;;; Class definitions
;;; -----------------

(defclass light ()
  ((node :initform nil ;;; the node to which light is attached
         :initarg :node
         :accessor light-node)
   (cone-angle :initform 0.0
               :initarg :cone-angle
               :accessor light-cone-angle)
   (color :initform nil
          :initarg :color
          :accessor light-color)
   (type :initform nil
         :initarg :type
         :accessor light-type)
   ;;; distance attentuation
   (kc-coeff :initform 1.0
             :initarg :kc-coeff
             :accessor light-const-att)
   (kd-coeff :initform 0.0
             :initarg :kd-coeff
             :accessor light-linear-att)
   (kq-coeff :initform 0.0
             :initarg :kq-coeff
             :accessor light-quad-att)
   (spot-att :initform 1.0
             :initarg :spot-att
             :accessor light-spot-att)))

(defun make-ambient-light (&optional (color (make-white-color)))
  (make-instance 'light
                 :type *ambient-light*
                 :color color))

(defun ambient? (light)
  (and light
       (equal (light-type light) *ambient-light*)))

(defun make-omni-light (&optional (color (make-white-color)))
  (make-instance 'light
                 :type *omni-light*
                 :color color))

(defun make-directional-light (&optional (color (make-white-color)))
  (make-instance 'light
                 :type *directional-light*
                 :color color))

(defun make-spot-light (&optional (color (make-white-color))
                                  (cone-angle 0.0))
  (make-instance 'light
                 :type *spot-light*
                 :cone-angle cone-angle
                 :color color))

(defun make-default-omni-light ()
  (let ((node (make-node))
        (light (make-omni-light (make-rgba 0.7 0.7 0.7))))
    (setf (node-position node) (make-vector3 0.0 0.0 2.0)
          (light-node light) node
          (node-light node) light)))

