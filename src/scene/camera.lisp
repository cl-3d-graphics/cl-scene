;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

(defconstant *default-aspect* 1.33) ;;; default aspect is 4:3

;;; -----------------
;;; Class definitions
;;; -----------------

(defclass camera ()
  (;;; perspective
   (near :initarg :z-near
         :initform 0.1
         :accessor camera-near)
   (far :initarg :z-far
        :initform 100.0
        :accessor camera-far)
   (fovy :initarg :fovy
         :initform (deg->rad 67.0)
         :accessor camera-fovy)
   (aspect :initarg :aspect
           :initform nil
           :accessor camera-aspect)
   ;;; transfrom
   (view-mat :initform nil
             :accessor camera-view-mat)
   (proj-mat :initform nil
             :accessor camera-proj-mat)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun make-default-camera ()
  (make-instance 'camera))

(defun make-camera (near far fovy)
  (make-instance 'camera
                 :z-near near
                 :z-far far
                 :fovy fovy))

(defun change-aspect (camera width height)
  (if (or (zerop height) (zerop width))
      (setf (camera-aspect camera) *default-aspect*)
      (setf (camera-aspect camera)
            (coerce (/ width (* 1.0 height)) 'float))))
