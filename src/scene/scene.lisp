;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

(defclass scene ()
  ((initialized :initarg :initialized
                :initform nil
                :accessor scene-initialized)
   (ambient-light-node :initform nil
                       :accessor scene-ambient-light-node)
   (light-nodes :initform nil
                :accessor scene-light-nodes)
   (use-default-light :initarg :use-default-light
                      :initform nil
                      :accessor scene-default-light)
   (point-of-view :initarg :point-of-view
                  :initform nil
                  :accessor scene-point-of-view)
   (root-node :initarg :root-node
              :initform nil
              :accessor scene-root-node)))

(defun make-default-camera-node ()
  (let ((camera-node (make-scene-node :name "Default Camera Node"
                                      :position (make-vector3 0.0 0.0 2.0)))
        (camera (make-default-camera)))
    (setf (node-camera camera-node) camera)
    camera-node))

(defun make-scene (&key (camera-node (make-default-camera-node)))
  (let ((root-node (make-instance 'scene-node)))
    (make-instance 'scene
                   :root-node root-node
                   :point-of-view camera-node)))

(defun add-light-node (scene node)
  (let ((light-nodes (scene-light-nodes scene)))
    (setf (scene-light-nodes scene)
          (append light-nodes (list node)))))

(defun add-node (scene child-node &optional parent-node)
  (let* ((source-node (if parent-node
                          parent-node
                          (scene-root-node scene)))
         (children (node-children source-node)))
    (setf (node-parent child-node)    source-node
          (node-children source-node) (append (list child-node)
                                              children))
    (let ((light (node-light child-node)))
      (when light
        (setf (light-node light) child-node)
        (if (ambient? light)
            (setf (scene-ambient-light-node scene) child-node)
            (add-light-node scene child-node))))))

(defun find-node (scene node-name &optional from-node)
  (let ((start-node (if from-node
                        from-node
                        (scene-root-node scene))))
    (labels ((find-node* (node)
               (format t "Checking node ~A with name ~A ~%"
                       node (node-name node))
               (if (equal (node-name node) node-name)
                   (return-from find-node node)
                   (dolist (n (node-children node))
                     (find-node* n)))))
      (find-node* start-node))))

(defun num-lights (scene)
  (count-if #'(lambda (node) (node-light node)) (scene-light-nodes scene)))

(defun scene-ambient-light (scene)
  (and (scene-ambient-light-node scene)
       (node-light (scene-ambient-light-node scene))))

(defun scene-lights (scene)
  (remove nil (mapcar #'node-light (scene-light-nodes scene))))

;;; ---------------------------------------------------------
;;; Makes 1 omni light that is used as default for both
;;; default lighting or when only 1 ambient light is assinged
;;; ---------------------------------------------------------
(defun make-default-lights ()
  (let ((omni-light (make-omni-light (make-rgba 0.7 0.7 0.7)))
        (omni-light-node (make-node)))
    (setf (node-position omni-light-node) (make-vector3 0.0 0.0 2.0)
          (node-light omni-light-node) omni-light)
    (setf (light-node omni-light) omni-light-node)
    (list omni-light)))

(defun scene-lighting? (scene)
  (let ((n-lights (num-lights scene)))
    (cond ((> n-lights 0) (values t *custom-lights* (scene-lights scene)))
          ((and (zerop n-lights) (scene-ambient-light-node scene))
           (values t *default-ambient* (make-default-lights)))
          ((and (zerop n-lights) (scene-default-light scene))
           (values t *default-light* (make-default-lights)))
          (t (values nil nil nil)))))
