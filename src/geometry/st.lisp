;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

;;; -----------------
;;; Class definitions
;;; -----------------

(defclass tex-st ()
  ((s-coord :initarg :s-coord
            :initform nil
            :accessor tex-s)
   (t-coord :initarg :t-coord
            :initform nil
            :accessor tex-t)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun make-tex-st (s-coord t-coord &optional flip-y)
  (make-instance 'tex-st
                 :s-coord (float s-coord)
                 :t-coord (if flip-y
                              (- 1.0 t-coord)
                              (float t-coord))))

(defun st->list (st)
  (if st
      (list (tex-s st)
            (tex-t st))))
