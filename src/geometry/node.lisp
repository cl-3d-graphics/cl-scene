;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

;;; -----------------
;;; Class definitions
;;; -----------------

(defclass scene-node ()
  ((name :initarg :name
         :initform "Unnamed-Node"
         :accessor node-name)
   (parent-node :initarg :parent-node
                :initform nil
                :accessor node-parent)
   (child-nodes :initarg :child-nodes
                :initform nil
                :accessor node-children)
   (camera :initarg :camera
           :initform nil
           :accessor node-camera)
   (light :initarg :light
          :initform nil
          :accessor node-light)
   (geometry :initarg :geometry
             :initform nil
             :accessor node-geometry)
   (position :initarg :position
             :initform nil
             :accessor node-position)
   (scale :initarg :scale
          :initform nil
          :accessor node-scale)
   (rotation :initarg :rotation
             :initform nil
             :accessor node-rotation)
   (orientation :initarg :orientation
                :initform nil
                :accessor node-orientation)
   ;;; maintain up, fwd, right vectors
   ;;; yaw, pitch, roll
   (up :initarg :up
       :initform (make-vector4 0.0 1.0 0.0 0.0)
       :accessor node-up)
   (right :initarg :fwd
          :initform (make-vector4 1.0 0.0 0.0 0.0)
          :accessor node-right)
   (fwd :initarg :fwd
        :initform (make-vector4 0.0 0.0 -1.0 0.0)
        :accessor node-fwd)
   ;;; OpenGL matrices
   (position-mat4 :initform nil
                  :accessor node-position-mat4)
   (scale-mat4 :initform nil
               :accessor node-scale-mat4)
   (rotation-mat4 :initform nil
                  :accessor node-rotation-mat4)
   (local-mat4 :initform nil
               :accessor node-local-mat4)
   (world-mat4 :initform nil
               :accessor node-world-mat4)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun make-node ()
  (make-instance 'scene-node :position (make-vector3)))

(defun make-scene-node (&key name geometry position)
  (let ((node (make-instance 'scene-node
                             :name name
                             :geometry geometry)))
    (if position (setf (node-position node) position))
    (setf (node-rotation node) (make-vector4))
    node))

;;; ---------------------------
;;; yaw, pitch, roll operations
;;; ---------------------------

(defun yaw (angle node)
  (let ((up (node-up node)))
    (setf (node-rotation node)
          (make-vector4 (vec4-x up) (vec4-y up) (vec4-z up) angle))))

(defun pitch (angle node)
  (let ((right (node-right node)))
    (setf (node-rotation node)
          (make-vector4 (vec4-x right) (vec4-y right) (vec4-z right) angle))))

(defun roll (angle node)
  (let ((fwd (node-fwd node)))
    (setf (node-rotation node)
          (make-vector4 (vec4-x fwd) (vec4-y fwd) (vec4-z fwd) angle))))

(defun unique-node-name ()
  (string (gensym "SK-NODE-")))
