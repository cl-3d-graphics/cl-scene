;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")


;;; -----------------
;;; Class definitions
;;; -----------------

(defclass scene-geometry ()
  ((name :initarg :name
         :initform "Unnamed-Geometry"
         :accessor geometry-name)
   (n-vertices :initarg :n-vertices
               :initform nil
               :accessor geometry-n-vertices)
   (vertices :initarg :vertices
             :initform nil
             :accessor geometry-vertices)
   (n-indices :initarg :n-indices
              :initform nil
              :accessor geometry-n-indices)
   (indices :initarg :indices
            :initform nil
            :accessor geometry-indices)
   (n-tri-faces :initarg :n-tri-faces
                :initform nil
                :accessor geometry-n-tri-faces)
   (tri-faces :initarg :tri-faces
              :initform nil
              :accessor geometry-tri-faces)
   (cull-face :initarg :cull-face
              :initform nil
              :accessor geometry-cull-face)
   (cull-back :initarg :cull-back-face
              :initform t
              :accessor geometry-cull-back-face)
   ;;; Represents the face winding dir
   (clockwise :initarg :clockwise
              :initform nil
              :accessor geometry-clockwise)
   (curr-attrib-pointer :initform 0
                        :accessor geometry-current-attrib-pointer)
   (vertex-source :initarg :vertex-source
                  :initform nil
                  :accessor geometry-vertex-source)
   (color-source :initarg :color-source
                 :initform nil
                 :accessor geometry-color-source)
   (normal-source :initarg :normal-source
                  :initform nil
                  :accessor geometry-normal-source)
   (tangent-source :initarg :tangent-source
                  :initform nil
                  :accessor geometry-tangent-source)
   (bi-tangent-source :initarg :bi-tangent-source
                      :initform nil
                      :accessor geometry-bi-tangent-source)
   (diffuse-source :initarg :diffuse-source
                   :initform nil
                   :accessor geometry-diffuse-source)
   (specular-source :initarg :specular-source
                    :initform nil
                    :accessor geometry-specular-source)
   (ambient-source :initarg :ambient-source
                   :initform nil
                   :accessor geometry-ambient-source)
   (emissive-source :initarg :emissive-source
                    :initform nil
                    :accessor geometry-emissive-source)
   (normal-tex-source :initarg :normal-tex-source
                      :initform nil
                      :accessor geometry-normal-tex-source)
   (index-source :initarg :index-source
                 :initform nil
                 :accessor geometry-index-source)
   (draw-type :initarg :draw-type
              :initform nil
              :reader geometry-draw-type)
   (shader-program :initarg :shader-program
                   :initform nil
                   :accessor geometry-shader-program)
   (material :initarg :material
             :initform nil
             :accessor geometry-material)
   ;;; OpenGL slots
   (vao :initarg :vao
        :initform nil
        :accessor geometry-vao)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun initialize-attrib-source (geometry list-fn attrib-fn n-comps
                                 source-fn source-slot-fn)
  (let ((source-data (apply #'append
                            (mapcar list-fn
                                    (mapcar attrib-fn
                                            (geometry-vertices geometry))))))
    (when source-data
      (let ((source (funcall source-fn
                             source-data
                             (geometry-n-vertices geometry)
                             *float*
                             n-comps)))
        (funcall source-slot-fn source geometry)))))

(defun initialize-index-source (geometry)
  (let* ((index-source (make-index-source (geometry-indices geometry)
                                          (geometry-n-indices geometry)
                                          *unsigned-int*
                                          1)))
    (setf (geometry-index-source  geometry) index-source)))

;;;-----------------------------------------------------------------------------
;;; Lengyel, Eric.
;;; “Computing Tangent Space Basis Vectors for an Arbitrary Mesh”.
;;; Terathon Software 3D Graphics Library, 2001.
;;; http://www.terathon.com/code/tangent.html
;;;-----------------------------------------------------------------------------

(defun compute-tangents (geometry)
  (when (and (geometry-normal-source geometry)
             (geometry-normal-tex-source geometry))
      (dolist (tri-face (geometry-tri-faces geometry))
        (with-slots (v1 v2 v3) tri-face
          (let* ((w1 (pos-normal-st v1))
                 (w2 (pos-normal-st v2))
                 (w3 (pos-normal-st v3))
                 (x1 (- (pos-x v2) (pos-x v1)))
                 (x2 (- (pos-x v3) (pos-x v1)))
                 (y1 (- (pos-y v2) (pos-y v1)))
                 (y2 (- (pos-y v3) (pos-y v1)))
                 (z1 (- (pos-z v2) (pos-z v1)))
                 (z2 (- (pos-z v3) (pos-z v1)))
                 (s1 (- (tex-s w2) (tex-s w1)))
                 (s2 (- (tex-s w3) (tex-s w1)))
                 (t1 (- (tex-t w2) (tex-t w1)))
                 (t2 (- (tex-t w3) (tex-t w1)))
                 (r (/ 1.0 (- (* s1 t2) (* s2 t1))))
                 (s-dir (make-vector3 (* (- (* t2 x1) (* t1 x2)) r)
                                      (* (- (* t2 y1) (* t1 y2)) r)
                                      (* (- (* t2 z1) (* t1 z2)) r)))
                 (t-dir (make-vector3 (* (- (* s1 x1) (* s2 x2)) r)
                                      (* (- (* s1 y1) (* s2 y2)) r)
                                      (* (- (* s1 z1) (* s2 z2)) r))))
            (setf (pos-tan1 v1) (vec3+ (pos-tan1 v1) s-dir)
                  (pos-tan1 v2) (vec3+ (pos-tan1 v2) s-dir)
                  (pos-tan1 v3) (vec3+ (pos-tan1 v3) s-dir)
                  (pos-tan2 v1) (vec3+ (pos-tan2 v1) t-dir)
                  (pos-tan2 v2) (vec3+ (pos-tan2 v2) t-dir)
                  (pos-tan2 v3) (vec3+ (pos-tan2 v3) t-dir)))))
      (dolist (v (geometry-vertices geometry))
        (let* ((normal (pos-normal v))
               (tan1   (pos-tan1 v))
               (det    (dot-vec3 (cross-vec3 normal tan1)
                                 (pos-tan2 v))))
          (setf (pos-tangent v)
                (normalize-vec3 (n*vec3 (dot-vec3 normal tan1)
                                        (vec3- tan1 normal))))
          (setf (pos-bi-tangent v)
                (n*vec3 (if (< det 0.0) -1.0 1.0)
                        (cross-vec3 normal (pos-tangent v))))))
      t))

(defmethod initialize-instance :after ((geometry scene-geometry)
                                       &rest args)
  (setf (geometry-n-vertices geometry) (length (geometry-vertices geometry))
        (geometry-n-indices geometry) (length (geometry-indices geometry))
        (geometry-n-tri-faces geometry) (length (geometry-tri-faces geometry)))
  (initialize-attrib-source geometry #'vertex3->list #'identity 3
                            #'make-vertex-source
                            #'(setf geometry-vertex-source))
  (initialize-attrib-source geometry #'vec3->list #'pos-normal 3
                            #'make-normal-source
                            #'(setf geometry-normal-source))
  (initialize-attrib-source geometry #'color->list #'pos-color 3
                            #'make-color-source
                            #'(setf geometry-color-source))
  (initialize-attrib-source geometry #'st->list #'pos-diffuse-st 2
                            #'make-diffuse-source
                            #'(setf geometry-diffuse-source))
  (initialize-attrib-source geometry #'st->list #'pos-specular-st 2
                            #'make-specular-source
                            #'(setf geometry-specular-source))
  (initialize-attrib-source geometry #'st->list #'pos-ambient-st 2
                            #'make-ambient-source
                            #'(setf geometry-ambient-source))
  (initialize-attrib-source geometry #'st->list #'pos-emissive-st 2
                            #'make-emissive-source
                            #'(setf geometry-emissive-source))
  (initialize-index-source geometry)
  (when (compute-tangents geometry)
    (initialize-attrib-source geometry #'vec3->list #'pos-tangent 3
                              #'make-tangent-source
                              #'(setf geometry-tangent-source))
    (initialize-attrib-source geometry #'vec3->list #'pos-bi-tangent 3
                              #'make-bi-tangent-source
                              #'(setf geometry-bi-tangent-source))
    (initialize-attrib-source geometry #'st->list #'pos-normal-st 2
                              #'make-normal-tex-source
                              #'(setf geometry-normal-tex-source))))
