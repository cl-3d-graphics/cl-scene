;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

(defconstant *rgba-comp-size* 4)

;;; -----------------
;;; Class definitions
;;; -----------------

(defclass texture ()
  ((texture-id :initarg :texture-id
               :initform nil
               :accessor texture-id)
   (active-slot :initarg :active-slot ;;; this refers to gl-active-texture slot
                :initform nil
                :accessor texture-active-slot)
   (path :initarg :path
         :initform nil
         :accessor texture-path)
   (size :initarg :size ;;; This is (* 4 height width)
         :initform nil
         :accessor texture-size)
   (height :initarg :height
           :initform nil
           :accessor texture-height)
   (width :initarg :width
          :initform nil
          :accessor texture-width)
   (gl-pixels :initarg :gl-pixels
              :initform nil
              :accessor texture-gl-pixels)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun make-texture (path)
  (make-instance 'texture :path path))
