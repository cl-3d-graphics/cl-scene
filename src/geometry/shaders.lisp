;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

;;; -----------------
;;; Class definitions
;;; -----------------

(defclass shader-program ()
  ((vertex-shader :initarg :vertex-shader
                  :initform nil
                  :reader shader-program-vertex-shader)
   (fragment-shader :initarg :fragment-shader
                    :initform nil
                    :reader shader-program-fragment-shader)
   (shader-locs-fn :initarg :shader-locs-fn
                   :initform nil
                   :accessor shader-program-locs-fn)
   ;;; OpenGL slots
   (vs :initarg :vs
       :initform nil
       :accessor shader-program-vs)
   (fs :initarg :fs
       :initform nil
       :accessor shader-program-fs)
   (sp :initarg :sp
       :initform nil
       :accessor shader-program-sp)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun shader-string (shader-dir shader-file)
  (file-string (merge-pathnames shader-file shader-dir)))

(defun make-shader-program (shader-dir vertex-shader fragment-shader locs-fn)
  (let ((vs (shader-string shader-dir vertex-shader))
        (fs (shader-string shader-dir fragment-shader)))
    (make-instance 'shader-program
                   :vertex-shader vs
                   :fragment-shader fs
                   :shader-locs-fn locs-fn)))
