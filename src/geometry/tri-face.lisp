;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

;;; -----------------
;;; Class definitions
;;; -----------------

(defclass tri-face ()
  ((v1 :initarg :v1
       :initform nil
       :accessor tri-face-v1)
   (v2 :initarg :v2
       :initform nil
       :accessor tri-face-v2)
   (v3 :initarg :v3
       :initform nil
       :accessor tri-face-v3)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun make-tri-face (v1 v2 v3)
  (make-instance 'tri-face
                 :v1 v1
                 :v2 v2
                 :v3 v3))


