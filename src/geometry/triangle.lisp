;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

(defun make-triangle (v1 v2 v3 &optional material)
  (let* ((tri-face (make-tri-face v1 v2 v3))
         (triangle (make-instance 'scene-geometry
                                  :name "Triangle"
                                  :vertices (list v1 v2 v3)
                                  :indices (list 0 1 2)
                                  :tri-faces (list tri-face)
                                  :cull-face t
                                  :cull-back-face t
                                  :clockwise t
                                  :draw-type *static-draw*)))
    (setf (geometry-material triangle) (if material
                                           material
                                           (make-default-material)))
    triangle))
