;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

(defconstant *vertex-source*     :vertex-source)
(defconstant *normal-source*     :normal-source)
(defconstant *color-source*      :color-source)
(defconstant *diffuse-source*    :diffuse-source)
(defconstant *specular-source*   :specular-source)
(defconstant *ambient-source*    :ambient-source)
(defconstant *emissive-source*   :emissive-source)
(defconstant *normal-tex-source* :normal-tex-source)
(defconstant *index-source*      :index-source)
(defconstant *tangent-source*    :tangent-source)
(defconstant *bi-tangent-source* :bi-tangent-source)

(defconstant *vertex-source-idx*     0)
(defconstant *index-source-idx*      1)
(defconstant *normal-source-idx*     2)
(defconstant *color-source-idx*      3)
(defconstant *diffuse-source-idx*    4)
(defconstant *specular-source-idx*   5)
(defconstant *ambient-source-idx*    6)
(defconstant *emissive-source-idx*   7)
(defconstant *normal-tex-source-idx* 8)
(defconstant *tangent-source-idx*    9)
(defconstant *bi-tangent-source-idx* 10)

;;; -----------------
;;; Class definitions
;;; -----------------

(defclass scene-geometry-source ()
  ((data :initarg :data
         :initform nil
         :accessor geometry-source-data)
   (count :initarg :count
          :initform nil
          :accessor geometry-source-count)
   (type :initarg :type
         :initform nil
         :accessor geometry-source-type)
   (data-type :initarg :data-type
              :initform nil
              :reader geometry-source-data-type)
   (n-per-vector :initarg :n-per-vector
                 :initform nil
                 :reader geometry-source-n-per-vector)
   ;;; OpenGL slots
   (index :initarg :index ; THIS IS VERTEX ATTRIB POINTER
          :initform nil
          :reader geometry-source-index)
   (vbo :initarg :vbo
        :initform nil
        :accessor geometry-source-vbo)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun make-geometry-source (data count type
                             data-type n-per-vector &optional index)
  (make-instance 'scene-geometry-source
                 :data data
                 :count count
                 :type type
                 :data-type data-type
                 :n-per-vector n-per-vector
                 :index index))

(defun make-vertex-source (data count data-type n-per-vector)
  (make-geometry-source data count
                        *vertex-source*
                        data-type
                        n-per-vector
                        *vertex-source-idx*))

(defun make-normal-source (data count data-type n-per-vector)
  (make-geometry-source data count
                        *normal-source*
                        data-type
                        n-per-vector
                        *normal-source-idx*))

(defun make-tangent-source (data count data-type n-per-vector)
  (make-geometry-source data count
                        *tangent-source*
                        data-type
                        n-per-vector
                        *tangent-source-idx*))

(defun make-bi-tangent-source (data count data-type n-per-vector)
  (make-geometry-source data count
                        *bi-tangent-source*
                        data-type
                        n-per-vector
                        *bi-tangent-source-idx*))

(defun make-color-source (data count data-type n-per-vector)
  (make-geometry-source data count
                        *color-source*
                        data-type
                        n-per-vector
                        *color-source-idx*))

(defun make-diffuse-source (data count data-type n-per-vector)
  (make-geometry-source data count
                        *diffuse-source*
                        data-type
                        n-per-vector
                        *diffuse-source-idx*))

(defun make-specular-source (data count data-type n-per-vector)
  (make-geometry-source data count
                        *diffuse-source*
                        data-type
                        n-per-vector
                        *specular-source-idx*))

(defun make-ambient-source (data count data-type n-per-vector)
  (make-geometry-source data count
                        *ambient-source*
                        data-type
                        n-per-vector
                        *ambient-source-idx*))

(defun make-emissive-source (data count data-type n-per-vector)
  (make-geometry-source data count
                        *emissive-source*
                        data-type
                        n-per-vector
                        *emissive-source-idx*))

(defun make-normal-tex-source (data count data-type n-per-vector)
  (make-geometry-source data count
                        *normal-tex-source*
                        data-type
                        n-per-vector
                        *normal-tex-source-idx*))

(defun make-index-source (data count data-type n-per-vector)
  (make-geometry-source data count
                        *index-source*
                        data-type
                        n-per-vector
                        *index-source-idx*))
