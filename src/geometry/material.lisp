;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

(defconstant *phong* "PHONG")
(defconstant *blinn-phong* "BLINN-PHONG") ;;; DEFUALT LIGHT MODEL

(defconstant *default-light* 0)
(defconstant *custom-lights* 1)
(defconstant *default-ambient* 2)


;;; -----------------
;;; Class definitions
;;; -----------------

(defclass material ()
  ((curr-active-tex-slot :initform 0
                         :initarg :curr-active-tex-slot
                         :accessor material-curr-active-tex-slot)
   (ambient :initform nil
            :initarg :ambient
            :accessor material-ambient)
   (diffuse :initform nil
            :initarg :diffuse
            :accessor material-diffuse)
   (emissive :initform nil
             :initarg :emissive
             :accessor material-emissive)
   (specular :initform nil
             :initarg :specular
             :accessor material-specular)
   (shininess :initform nil
              :initarg :shininess
              :accessor material-shininess)
   (normal :initform nil
           :initarg :normal
           :accessor material-normal)
   (light-model :initform *phong*
                :initarg :light-model
                :accessor material-light-model)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun get-light-model-id (light-model)
  (cond ((equal light-model *blinn-phong*) 1)
        ((equal light-model *phong*) 2)
        (t nil)))

(defun make-default-material ()
  (let ((material (make-instance 'material)))
    (setf (material-ambient material) (make-dark-gray-color)
          (material-diffuse material) (make-white-color)
          (material-emissive material) (make-black-color)
          (material-specular material) (make-black-color)
          (material-normal material) (make-white-color)
          (material-shininess material) 33.0
          (material-light-model material) *blinn-phong*)
    material))

(defun make-material (emissive ambient diffuse specular shininess)
  (let ((material (make-instance 'material)))
    (setf (material-emissive material) emissive
          (material-ambient material) ambient
          (material-diffuse material) diffuse
          (material-specular material) specular
          (material-normal material) (make-white-color)
          (material-shininess material) shininess
          (material-light-model material) *blinn-phong*)
    material))

(defmethod (setf material-diffuse) ((new-color color-rgba) (material material))
  (setf (slot-value material 'diffuse) new-color))

(defmethod (setf material-diffuse) ((new-texture texture) (material material))
  (setf (slot-value material 'diffuse) new-texture
        (texture-active-slot new-texture) (material-curr-active-tex-slot
                                            material))
  (incf (material-curr-active-tex-slot material)))

(defmethod (setf material-specular) ((new-color color-rgba) (material material))
  (setf (slot-value material 'specular) new-color))

(defmethod (setf material-specular) ((new-texture texture) (material material))
  (setf (slot-value material 'specular) new-texture
        (texture-active-slot new-texture) (material-curr-active-tex-slot
                                           material))
  (incf (material-curr-active-tex-slot material)))

(defmethod (setf material-ambient) ((new-color color-rgba) (material material))
  (setf (slot-value material 'ambient) new-color))

(defmethod (setf material-ambient) ((new-texture texture) (material material))
  (setf (slot-value material 'ambient) new-texture
        (texture-active-slot new-texture) (material-curr-active-tex-slot
                                           material))
  (incf (material-curr-active-tex-slot material)))

(defmethod (setf material-emissive) ((new-color color-rgba) (material material))
  (setf (slot-value material 'emissive) new-color))

(defmethod (setf material-emissive) ((new-texture texture) (material material))
  (setf (slot-value material 'emissive) new-texture
        (texture-active-slot new-texture) (material-curr-active-tex-slot
                                           material))
  (incf (material-curr-active-tex-slot material)))

(defmethod (setf material-normal) ((new-color color-rgba) (material material))
  (setf (slot-value material 'normal) new-color))

(defmethod (setf material-normal) ((new-texture texture) (material material))
  (setf (slot-value material 'normal) new-texture
        (texture-active-slot new-texture) (material-curr-active-tex-slot
                                           material))
  (incf (material-curr-active-tex-slot material)))
