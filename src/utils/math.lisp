;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

(defun deg->rad (deg)
  ;;; coerce to single float as our gl vectors are single floats
  (coerce (/ (* pi deg) 180.0) 'single-float))

