;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

;;; -----------------
;;; Class definitions
;;; -----------------

(defclass color-rgba ()
  ((r :initarg :r
      :initform nil
      :accessor color-rgba-r)
   (g :initarg :g
      :initform nil
      :accessor color-rgba-g)
   (b :initarg :b
      :initform nil
      :accessor color-rgba-b)
   (a :initarg :a
      :initform nil
      :accessor color-rgba-a)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun make-rgba (r g b &optional (a 1.0))
  (make-instance 'color-rgba
                 :r r
                 :g g
                 :b b
                 :a a))

(defun make-black-color ()
  (make-rgba 0.0 0.0 0.0))

(defun make-white-color ()
  (make-rgba 1.0 1.0 1.0))

(defun make-dark-gray-color ()
  (let ((c (/ 166.0 255.0)))
    (make-rgba c c c)))

(defun color->list (color)
  (if color
      (list (color-rgba-r color)
            (color-rgba-g color)
            (color-rgba-b color))))

(defun color-spec->color-rgba (color-spec)
  (make-instance 'color-rgba
                 :r (aref color-spec 1)
                 :g (aref color-spec 2)
                 :b (aref color-spec 3)
                 :a (aref color-spec 4)))
