;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

;;; -----------------
;;; Class definitions
;;; -----------------

(defclass vector3 ()
  ((x :initarg :x
      :initform nil
      :accessor vec3-x)
   (y :initarg :y
      :initform nil
      :accessor vec3-y)
   (z :initarg :z
      :initform nil
      :accessor vec3-z)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun make-vector3 (&optional (x 0) (y 0) (z 0))
  (make-instance 'vector3
                 :x (float x)
                 :y (float y)
                 :z (float z)))

(defun vec3+ (v1 v2)
  (make-instance 'vector3
                 :x (+ (vec3-x v1) (vec3-x v2))
                 :y (+ (vec3-y v1) (vec3-y v2))
                 :z (+ (vec3-z v1) (vec3-z v2))))

(defun vec3- (v1 v2)
  (make-instance 'vector3
                 :x (- (vec3-x v1) (vec3-x v2))
                 :y (- (vec3-y v1) (vec3-y v2))
                 :z (- (vec3-z v1) (vec3-z v2))))

(defun n*vec3 (n v)
  (make-instance 'vector3
                 :x (* n (vec3-x v))
                 :y (* n (vec3-y v))
                 :z (* n (vec3-z v))))

(defun negate-vector3 (vec3)
  (make-vector3 (* -1.0 (vec3-x vec3))
                (* -1.0 (vec3-y vec3))
                (* -1.0 (vec3-z vec3))))

(defun distance-vec3 (source target)
  (apply #'make-vector3
         (mapcar #'- (vec3->list target) (vec3->list source))))

(defun mag-vec3 (vec3)
  (with-slots (x y z) vec3
    (sqrt (+ (* x x) (* y y) (* z z)))))

(defun normalize-vec3 (vec3)
  (let ((m (mag-vec3 vec3)))
    (make-vector3 (/ (vec3-x vec3) m)
                  (/ (vec3-y vec3) m)
                  (/ (vec3-z vec3) m))))

(defun dot-vec3 (v1 v2)
  (+ (* (vec3-x v1) (vec3-x v2))
     (* (vec3-y v1) (vec3-y v2))
     (* (vec3-z v1) (vec3-z v2))))

(defun cross-vec3 (vec3-a vec3-b)
  (with-slots ((x1 x) (y1 y) (z1 z)) vec3-a
    (with-slots ((x2 x) (y2 y) (z2 z)) vec3-b
      (make-vector3 (- (* y1 z2) (* z1 y2))
                    (- (* z1 x2) (* x1 z2))
                    (- (* x1 y2) (* y1 x2))))))

(defun vec3->list (vertex3)
  (if vertex3
      (list (vec3-x vertex3)
            (vec3-y vertex3)
            (vec3-z vertex3))))
