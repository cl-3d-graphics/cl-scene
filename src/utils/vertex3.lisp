;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

;;; -----------------
;;; Class definitions
;;; -----------------

(defclass vertex3 ()
  ((x :initarg :x
      :initform nil
      :accessor pos-x)
   (y :initarg :y
      :initform nil
      :accessor pos-y)
   (z :initarg :z
      :initform nil
      :accessor pos-z)
   (color :initarg :color
          :initform nil
          :accessor pos-color)
   (normal :initarg :normal
           :initform nil
           :accessor pos-normal)
   (tan1 :initarg :tan1
         :initform (make-vector3)
         :accessor pos-tan1)
   (tan2 :initarg :tan2
         :initform (make-vector3)
         :accessor pos-tan2)
   (tangent :initarg :tangent
            :initform nil
            :accessor pos-tangent)
   (bi-tangent :initarg :tangent
               :initform nil
               :accessor pos-bi-tangent)
   (diffuse-st :initarg :diffuse-st
               :initform nil
               :accessor pos-diffuse-st)
   (specular-st :initarg :specular-st
                :initform nil
                :accessor pos-specular-st)
   (ambient-st :initarg :ambient-st
                :initform nil
               :accessor pos-ambient-st)
   (emissive-st :initarg :emissive-st
                :initform nil
                :accessor pos-emissive-st)
   (normal-st :initarg :normal-st
              :initform nil
              :accessor pos-normal-st)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun make-vertex3 (&optional (x 0.0) (y 0.0) (z 0.0) normal)
  (make-instance 'vertex3
                 :x x
                 :y y
                 :z z
                 :normal normal))

(defun vertex3->list (vertex3)
  (list (pos-x vertex3)
        (pos-y vertex3)
        (pos-z vertex3)))
