;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")


;;; -----------------
;;; Class definitions
;;; -----------------

(defclass vector4 ()
  ((x :initarg :x
      :initform nil
      :accessor vec4-x)
   (y :initarg :y
      :initform nil
      :accessor vec4-y)
   (z :initarg :z
      :initform nil
      :accessor vec4-z)
   (w :initarg :w
      :initform nil
      :accessor vec4-w)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun make-vector4 (&optional (x 0) (y 0) (z 0) (w 0))
  (make-instance 'vector4
                 :x (float x)
                 :y (float y)
                 :z (float z)
                 :w (float w)))
