;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

;;; -----------------
;;; Class definitions
;;; -----------------

(defclass versor ()
  ((q0 :initarg :q0
       :initform nil
       :accessor versor-q0)
   (q1 :initarg :q1
       :initform nil
       :accessor versor-q1)
   (q2 :initarg :q2
       :initform nil
       :accessor versor-q2)
   (q3 :initarg :q3
       :initform nil
       :accessor versor-q3)))

;;; -----------------
;;; Class methods
;;; -----------------

(defun make-versor (q0 q1 q2 q3)
  (make-instance 'versor
                 :q0 q0
                 :q1 q1
                 :q2 q2
                 :q3 q3))

(defun rotation-to-quat (rotation)
  (let ((cos-w/2 (cos (/ (vec4-w rotation) 2.0)))
        (sin-w/2 (sin (/ (vec4-w rotation) 2.0))))
    (make-instance 'versor
                   :q0 cos-w/2
                   :q1 (* (vec4-x rotation) sin-w/2)
                   :q2 (* (vec4-y rotation) sin-w/2)
                   :q3 (* (vec4-z rotation) sin-w/2))))

(defun normalize-quat (quat)
	;; norm(q) = q / magnitude (q)
	;; magnitude (q) = sqrt (w*w + x*x...)
	;; only compute sqrt if interior sum != 1.0
  (with-slots (q0 q1 q2 q3) quat
    (let ((sum (+ (* q0 q0) (* q1 q1) (* q2 q2) (* q3 q3)))
          (thresh 0.0001))
      (if (< (abs (- 1.0 sum)) thresh)
          quat
          (let ((mag (sqrt sum)))
            (make-versor (/ q0 mag)
                         (/ q1 mag)
                         (/ q2 mag)
                         (/ q3 mag)))))))

(defun quat* (s r)
  (with-slots ((s0 q0) (s1 q1) (s2 q2) (s3 q3)) s
    (with-slots ((r0 q0) (r1 q1) (r2 q2) (r3 q3)) r
      (normalize-quat
        (make-versor (- (* s0 r0) (* s1 r1) (* s2 r2) (* s3 r3))
                    (- (+ (* s0 r1) (* s1 r0) (* s3 r2)) (* s2 r3))
                    (- (+ (* s0 r2) (* s1 r3) (* s2 r0)) (* s3 r1))
                    (- (+ (* s0 r3) (* s2 r1) (* s3 r0)) (* s1 r2)))))))
