;;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "SCENE")

;;; ----------
;;; data types
;;; ----------

(defconstant *float* :float)
(defconstant *double* :double)
(defconstant *unsigned-short* :unsigned-short)
(defconstant *unsigned-int* :unsigned-int)

;;; ----------
;;; draw types
;;; ----------

(defconstant *static-draw*  :static-draw)
(defconstant *dynamic-draw* :dynamic-draw)
(defconstant *stream-draw*  :stream-draw)
