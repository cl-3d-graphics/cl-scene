;; Copyright (c) 2015 Deepak Surti. All rights reserved.

This is a scene graph library, which is independent of a rendering component. As
such it is cross platform and can be used on desktop, mobile and the web.

This library provides:
* A scene class which represents the scene graph
* A scene node class whose instances make up the scene

In order to render/draw this scene graph, you can use the `lw-capi-3d-kit`
library to draw into an OpenGL canvas using Lispworks CAPI. Please note that
`lw-capi-3d-kit` is LW CAPI specific. It serves as a reference implementation.
It should be relatively straight forward to render this scene graph using any
other GUI library for the desktop.

For mobile or the web, only an equivalent scene graph data structure
implementation is required on that platform. This native scene graph than be
constructed usig this Lisp scene graph. This will allow easy computation of the
OpenGL specific data structures (matrices, vectors etc) that are required at
render time.

The platform specific render loop can then be used to update the
Lisp scene graph and synced again with the native scene graph. This
synchronization will need optimization. Overall writing a compatible scene graph
with initialization, synchronization is much less effort and will allow complex
3D apps such as games, scientific visualization apps developed in Lisp to be
used across multi platforms such as desktop, mobile and web with maximum code
reuse.

This strategy seems to be the most pragmatic given the diffucilty of writing
cross platform graphics applications.