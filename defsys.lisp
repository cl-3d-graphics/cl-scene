;; Copyright (c) 2015 Deepak Surti. All rights reserved.

(in-package "USER")

(defsystem "SCENE"
  (:optimize ((debug 3) (safety 3)))
  :members ("pkg"
            "src/utils/color"
            "src/utils/math"
            "src/utils/types"
            "src/utils/vertex3"
            "src/utils/vector3"
            "src/utils/vector4"
            "src/utils/quat"
            "src/geometry/st"
            "src/geometry/texture"
            "src/geometry/material"
            "src/geometry/tri-face"
            "src/geometry/source"
            "src/geometry/geometry"
            "src/geometry/shaders"
            "src/geometry/node"
            "src/geometry/triangle"
            "src/scene/light"
            "src/scene/camera"
            "src/scene/scene")
  :rules ((:in-order-to :load :all
                        (:requires (:load :serial)))
          (:in-order-to :compile :all
                        (:caused-by (:compile :previous))
                        (:requires (:load :serial))))
  )



